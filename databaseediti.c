#include<stdio.h>
#include<string.h>
struct student
{
	int phy,chem,math;
	char name[128];
}s[100],t;
int input();
void arrange(int);
void output(int);
void swap(int);
int main()
{ 
    int n;
	n=input();
	printf("The student database before arranging\n");
	output(n);
    arrange(n);
    printf("The student database after arranging\n");
    output(n);
    return 0;
}
int input()
{   
    int n,i;
    printf("Enter the number of students\n");
    scanf("%d",&n);
    for(i=0;i<n;i++)
    {  
       printf("Enter the name of the student\n");
       scanf("%s",s[i].name);
       printf("Enter the marks of the students\n");
       printf("Physics mark,Chemistry mark and Maths mark\n");
       scanf("%d %d %d",&s[i].phy,&s[i].chem,&s[i].math);
    }
    return n;
}
void swap(int j)
{   
    t=s[j];
    s[j]=s[j+1];
    s[j+1]=t;
}
void arrange(int n)
{
    int i,j;
    for(i=0;i<n;i++)
   {
      for (j=0;i<n-1;i++)
       {
         if(s[j].phy<s[j+1].phy)
         swap(j);
         else if(s[j].phy==s[j+1].phy)
         {
           if(s[j].chem<s[j+1].chem)
             swap(j);
           else if(s[j].chem==s[j+1].chem)
           {
             if(s[j].math<s[j+1].math)
             swap(j);
             else if(s[j].math==s[j+1].math)
            {
              if(strcmp(s[j].name,s[j+1].name)>0)
              swap(j);
            }
          }
         }
       }
    }
}
void output(int n)
{  
   int i;
   printf("Name\tPhysics marks\tChemistry marks\t\tMaths marks\n");
   for(i=0;i<n;i++)
     printf("%s\t%d\t\t%d\t\t\t%d\n",s[i].name,s[i].phy,s[i].chem,s[i].math);
}
