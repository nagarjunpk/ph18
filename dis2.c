
#include <stdio.h>
#include<math.h>
void input(int *x1,int *y1,int *x2,int *y2)
{
	printf("Enter the intial point(x1,y1)\n");
	scanf("%d %d",x1,y1);
	printf("Enter the final point(x2,y2)\n");
	scanf("%d %d",x2,y2);

}
float compute(int x1,int y1,int x2,int y2)
{
	float dis;
	dis=sqrt(pow((x2-x1),2)+pow((y2-y1),2));
	return dis;
	
}
void output(int x1,int y1,int x2,int y2,float dis)
{
	printf("The distance between (%d,%d) and (%d,%d) is %f",x1,y1,x2,y2,dis);
}
int main()
{ 
	int x1,x2,y1,y2;
	float dis;
	input(&x1,&y1,&x2,&y2);
	dis=compute(x1,y1,x2,y2);
	output(x1,y1,x2,y2,dis);
}

