#include <stdio.h>
void input(int *a,int *b)
{
	printf("Enter the two numbers to be added\n");
	scanf("%d %d",a,b);
}
void compute(int *a, int *b,int *sum)
{
	*sum=*a+*b;
	
}
void output(int *a,int *b,int *sum)
{
	printf("The sum of the entered two numbers \'%d\' and \'%d\' is %d",*a,*b,*sum);
}
int main()
{ 
	int a,b,sum;
	input(&a,&b);
	compute(&a,&b,&sum);
	output(&a,&b,&sum);
}
