#include <stdio.h>
void input(int *);
int length(int);
void thousand(int *,int *);
void two(int *,int *);
void one(int*);
int main()
{
	int num,len=0;
	input(&num); 
	len=length(num);
	 if(len>=3)
	 {
	    thousand(&num,&len);
	  	while(len!=0)
	   {   
	      if(len>=2)
	       two(&num,&len);
	      else
	      {
	       one(&num);
	       break;
	      }
	   }
	 }
    return 0;
}
void input(int *num)
{
  printf("Enter the number less then 100000000(8 zeros)\n");
  scanf("%d",num);
}
int length(int num)
{   
    int n=0;
    while(num!=0)
	{
	 num=num/10;
	 n++;
	}
	return n;
}
void thousand(int *num,int *len)
{       
        int rem;
        rem=*num%1000;
		*num=(*num-rem)/1000;
		*len=*len-3;   
		printf("%d\n",rem);
}
void two(int *num,int *len)
{       
        int rem; 
        rem=*num%100;
        *num=(*num-rem)/100;
        *len=*len-2;
        printf("%d\n",rem);
}
void one(int *num)
{
    int rem;
    rem=*num%10;
    *num=(*num-rem)/10;
    printf("%d",rem);
}

